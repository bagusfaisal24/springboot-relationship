package com.alterra.springbootrelationship.domain.dto;

import lombok.Data;

@Data
public class TokenResponse {
    private String token;
}
