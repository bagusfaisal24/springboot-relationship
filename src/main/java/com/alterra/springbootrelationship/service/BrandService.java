package com.alterra.springbootrelationship.service;

import com.alterra.springbootrelationship.constant.AppConstant.ResponseCode;
import com.alterra.springbootrelationship.domain.dao.Brand;
import com.alterra.springbootrelationship.domain.dto.BrandDto;
import com.alterra.springbootrelationship.repository.BrandRepository;
import com.alterra.springbootrelationship.util.ResponseUtil;

import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import lombok.extern.slf4j.Slf4j;

import java.util.List;

@Slf4j
@Service
public class BrandService {
    
    @Autowired
    private BrandRepository brandRepository;

    @Autowired
    private ModelMapper mapper;

    public ResponseEntity<Object> addBrand(BrandDto request) {
        log.info("Executing save new brand");
        try {
            Brand brand = mapper.map(request, Brand.class);
            brandRepository.save(brand);
            return ResponseUtil.build(ResponseCode.SUCCESS, mapper.map(brand, BrandDto.class), HttpStatus.OK);
        } catch (Exception e) {
            log.error("Got an error when saving new brand. Error: {}", e.getMessage());
            return ResponseUtil.build(ResponseCode.UNKNOWN_ERROR, null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    public ResponseEntity<Object> getBrand() {
        log.info("Executing save new brand");
        try {
            List<Brand> brands = brandRepository.findAll();
            return ResponseUtil.build(ResponseCode.SUCCESS, brands, HttpStatus.OK);
        } catch (Exception e) {
            log.error("Got an error when saving new brand. Error: {}", e.getMessage());
            return ResponseUtil.build(ResponseCode.UNKNOWN_ERROR, null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

}
