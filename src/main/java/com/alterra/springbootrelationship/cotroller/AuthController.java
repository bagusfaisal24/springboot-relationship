package com.alterra.springbootrelationship.cotroller;

import com.alterra.springbootrelationship.constant.AppConstant;
import com.alterra.springbootrelationship.domain.dao.Brand;
import com.alterra.springbootrelationship.domain.dao.User;
import com.alterra.springbootrelationship.domain.dto.LoginForm;
import com.alterra.springbootrelationship.service.AuthService;
import com.alterra.springbootrelationship.util.ResponseUtil;
import lombok.RequiredArgsConstructor;
import org.modelmapper.ModelMapper;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping(value = "/v1/auth")
@RequiredArgsConstructor
public class AuthController {

    private final AuthService userService;

    @PostMapping("/register")
    public ResponseEntity<Object> register (@RequestBody LoginForm form){
       return userService.register(form);
    }

    @PostMapping("/token")
    public ResponseEntity<?> getToken (@RequestBody LoginForm form){
        return ResponseEntity.ok(userService.generateToken(form));
    }
}
