package com.alterra.springbootrelationship.cotroller;

import com.alterra.springbootrelationship.domain.dto.BrandDto;
import com.alterra.springbootrelationship.service.BrandService;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping(value = "/v1/brand")
public class BrandController {

    @Autowired
    private BrandService brandService;

    @PostMapping(value = "")
    public ResponseEntity<Object> createNewBrand(@RequestBody BrandDto request) {
        return brandService.addBrand(request);
    }

    @GetMapping(value = "")
    public ResponseEntity<Object> getAllBrand() {
        return brandService.getBrand();
    }
    
}
